﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightAPIApplication.hitchhiker;

namespace FlightAPIApplication
{
    class Program
    {
        static hitchhiker.FlightAPIClient fapi = new hitchhiker.FlightAPIClient("http");
        static hitchhiker.LoginData login = new hitchhiker.LoginData { UserName = "Tripchamp.test", Password = "456tr!ßip!_cg!" };

        static void Main(string[] args)
        {


            StandardRequest sr = new StandardRequest();
            sr.From = "SEA";
            sr.To = "LAX";
            sr.TravellersAdults = 2;
            sr.Class = ClassTypeEnum.Economy;
            sr.DepartureDate = new DateTime(2014, 10, 26);
            sr.ProductSubType = StandardRequest.ProductSubTypes.OneWay;


            FareResponseData fareResponse = getFares(sr);

            /*Output from GetFares*/


            Console.WriteLine("Number of fares returned by getFares:" + fareResponse.NumberOfFares);



            /*First Flight from Fare Search is selected*/


            string selectedFlight = fareResponse.Fares[0].Flight.Legs[0].Recordset;
            string selectedCarrier = fareResponse.Fares[0].Flight.Legs[0].CarrierCode;

            Console.WriteLine("Retrieving result of First Flight from FareSearch");
            Console.WriteLine("Flight" + selectedFlight);
            Console.WriteLine("Carrier:" + selectedFlight);

            //Output from getRuleInformation
            RuleTextResponseData ruleResponse = getRuleInformation(selectedFlight);
            Console.WriteLine("RuleInformationText:" + ruleResponse.RecordSet);

            //Output from GetTermsAndConditions
            TermsAndConditionsResponseData termsAndConditionsResponse = getTermsAndConditions(selectedCarrier, selectedFlight);
            Console.WriteLine("TermsAndConditions\n" + termsAndConditionsResponse.Text);


            //Output from GetBoardingdetails
            BoardingResponseData boardingData = getBoarding(selectedCarrier, sr.From, sr.DepartureDate, selectedFlight);

            Console.WriteLine("Number of Bags:" + boardingData.BoardingDetails[0].Baggages[2].NumberOfPieces);
            Console.WriteLine("Max Piece Weight:" + boardingData.BoardingDetails[0].Baggages[2].MaxPieceWeight);
            Console.WriteLine("Baggage Fee:$" + boardingData.BoardingDetails[0].Baggages[2].BaggageFee.Amount.Value);

            //Output from getPaymentMethods

            PaymentMethods paymentMethods = getPaymentMethods(selectedCarrier, sr.From, sr.DepartureDate, selectedFlight);
            Console.WriteLine("Payment Type:" + paymentMethods.PaymentDetails[0].PaymentType.ToString());
            Console.WriteLine("Booking Fee:" + paymentMethods.PaymentDetails[0].BookingFee.Amount.Value);


            BookResponseData bookResponse = bookFare(selectedCarrier, selectedFlight, sr.From, sr.DepartureDate);
            //Output from bookFare
            Console.WriteLine("Passenger First Name:" + bookResponse.Passengers[0].FirstName);
            Console.WriteLine("Passenger Last Name:" + bookResponse.Passengers[0].LastName);
            Console.WriteLine("Passenger Type:" + bookResponse.Passengers[0].PassengerType.ToString());
            Console.WriteLine("Total Tax:" + bookResponse.Passengers[0].TotalTax.Value);

            Console.Read();

        }

        //getFares
        public static FareResponseData getFares(StandardRequest sr)
        {


            FareRequestData frd = new FareRequestData();

            //ppass passenger info
            frd.Passengers = new FareRequestPassenger[]{
                new FareRequestPassenger()
				{Number=sr.TravellersAdults,Type=PassengerTypeEnum.Adult},
                new FareRequestPassenger()
				{Number=sr.TravellersKids,Type=PassengerTypeEnum.Child},
                new FareRequestPassenger()
				{Number=sr.TravellersSeniors,Type=PassengerTypeEnum.Senior},
                new FareRequestPassenger(){
				Number=sr.TravellersInfants,Type=PassengerTypeEnum.Infant} };

            //oneway and return

            switch (sr.ProductSubType)
            {
                case StandardRequest.ProductSubTypes.OneWay:
                    {
                        frd.Legs = new FareRequestLeg[1] {
                new FareRequestLeg(){Departures = new string[]{sr.From},
				DepartureDate =sr.DepartureDate ,Arrivals = new string[] {sr.To},
				DepartureDateSpecified=true,Class=sr.Class    },
                 };

                        break;
                    }
                case StandardRequest.ProductSubTypes.Return:
                    {
                        frd.Legs = new FareRequestLeg[2] {
                new FareRequestLeg(){Departures = new string[]{sr.From},
				DepartureDate =sr.DepartureDate ,Arrivals = new string[] {sr.To},
				DepartureDateSpecified=true,Class=sr.Class  },
                new FareRequestLeg(){Departures = new
				string[]{sr.To},DepartureDate =sr.ArrivalDate ,Arrivals = new string[]
				{sr.From}, DepartureDateSpecified=true,Class=sr.Class }    };

                        break;
                    }
                case StandardRequest.ProductSubTypes.MultiCity:
                case StandardRequest.ProductSubTypes.OpenJaw:
                    {
                        if (sr.MultipleLegs == null ||
                sr.MultipleLegs.Count() == 0)
                        {
                            //throw error Error("Multi city infromation not found");
                        }
                        frd.Legs = new
                FareRequestLeg[sr.MultipleLegs.Count()];

                        for (int i = 0; i < sr.MultipleLegs.Count(); i++)
                        {
                            frd.Legs[i] = new FareRequestLeg()
                            {
                                Departures
                                    = new string[] { sr.MultipleLegs[i].From },
                                DepartureDate =
                                    sr.MultipleLegs[i].DepartureDate,
                                Arrivals = new string[] {
								sr.MultipleLegs[i].To },
                                DepartureDateSpecified = true,
                                Class =
                                    (sr.MultipleLegs[i].Class)
                            };
                        }


                        break;
                    }

            }

            login.TransactionID = new Guid().ToString();

            frd.Carriers = new FareRequestCarrier[1];
            frd.Configuration = new FareRequestConfiguration();
            frd.Configuration.CurrencyCode = "USD";
            frd.Configuration.PaymentInfo = FareTypeEnum.All;
            frd.Configuration.FareDiversity = FareDivsersityEnum.AirlineDiversity;
            frd.Configuration.PaymentInfo = FareTypeEnum.All;
            frd.Configuration.ReturnNonStopFlightsOnly = NonStopEnum.AllFlight;
            frd.Configuration.LanguageCode = "en";
            frd.Fare = new FareRequestFare();
            frd.Fare.Web = new FareRequestWeb();
            frd.Fare.Charter = new FareRequestCharter();
            frd.Fare.GDS = new GDSFares();
            frd.Fare.Net = new FareRequestNet();
            frd.Fare.Vayant = new FareRequestVayant();
            frd.Pricing = new FareRequestPricing();



            frd.Configuration.AllotmentConfig = new AllotmentRequest();
            frd.Configuration.AllotmentConfig.Priority = AllotmentPriorityEnum.All;
            frd.Configuration.AllotmentConfig.EnhancedSearch = EnhancedAllotmentEnum.On;
            frd.Fare.Charter.NumberOfAvailableFares = 0;
            frd.Fare.Web.NumberOfWebFares = 30;
            frd.Fare.GDS.NumberOfFares = 0;
            frd.Fare.GDS.GDSTypes = GDSTypeEnum.Published;
            frd.Fare.Net.NumberOfNetFares = 0;
            frd.Fare.Vayant.NumberOfFares = 0;

            frd.Configuration.CRSConfig = new CRS();
            frd.Configuration.CRSConfig.TerminalCountry = "us";
            frd.Configuration.CRSConfig.CRSName = GDSEnum.AmadeusAPI;
            frd.Configuration.CRSConfig.TerminalPCC = "FRAL121BX";
            frd.Configuration.CRSConfig.ReceivedFrom = "FAPI TEST TOOL";



            var result = fapi.GetFares(login, frd);

            return result;

        }

        //GetRuleInformationText
        public static RuleTextResponseData getRuleInformation(String recordSet)
        {
            RuleTextRequestData rrd = new RuleTextRequestData();
            rrd.CRSConfig = new CRS();
            rrd.CurrencyCode = "USD";
            rrd.LanguageCode = "en";
            rrd.RecordSet = recordSet;
            rrd.Source = SourceModuleEnum.WebSource;
            rrd.RuleTypeSpecified = false;
            return fapi.GetRuleInformationText(login, rrd);
        }

        //GetTermsAndConditions
        public static TermsAndConditionsResponseData getTermsAndConditions(String carrierCode, String recordSet)
        {
            TermsAndConditionsRequestData tacr = new TermsAndConditionsRequestData();
            tacr.CarrierCode = carrierCode;
            tacr.LanguageCode = "en";
            tacr.RecordSet = recordSet;
            tacr.Source = SourceModuleEnum.WebSource;
            return fapi.GetTermsAndConditions(login, tacr);
        }


        //Get Boarding Details
        public static BoardingResponseData getBoarding(String carrierCode, String departure, DateTime departureDate, String recordSet)
        {
            BoardingRequestData brd = new BoardingRequestData();
            brd.CarrierCode = carrierCode;
            brd.Configuration = new BoardingRequestConfiguration();
            brd.CurrencyCode = "USD";
            brd.Departure = departure;
            brd.DepartureDate = departureDate;
            brd.DepartureDateSpecified = true;
            brd.LanguageCode = "en";
            brd.Recordset = recordSet;
            brd.Source = SourceModuleEnum.WebSource;

            return fapi.GetBoardingDetails(login, brd);
        }

        //GetPaymentMethods
        public static PaymentMethods getPaymentMethods(String carrierCode, String departure, DateTime departureDate, String recordSet)
        {
            PaymentRequestData prd = new PaymentRequestData();
            prd.CarrierCode = carrierCode;
            prd.CurrencyCode = "USD";
            prd.Departure = departure;
            prd.DepartureDate = departureDate;
            prd.DepartureDateSpecified = true;
            prd.LanguageCode = "en";
            prd.Recordset = recordSet;
            prd.Source = SourceModuleEnum.WebSource;

            return fapi.GetPaymentMethods(login, prd);
        }

        //BookFare
        public static BookResponseData bookFare(String carrier, String recordset, String departure, DateTime departureDate)
        {
            BookRequestData brd = new BookRequestData();
            brd.Configuration = new BookRequestConfiguration();
            brd.Configuration.IsLiveBooking = false;
            brd.Configuration.RecordSet = recordset;
            brd.Configuration.CRSConfig = new BookRequestCRSConfiguration();
            brd.Configuration.CRSConfig.TerminalCountry = "us";
            brd.Configuration.CRSConfig.CRSName = GDSEnum.AmadeusAPI;
            brd.Configuration.CRSConfig.TerminalPCC = "FRAL121BX";
            brd.Configuration.CRSConfig.ReceivedFrom = "FAPI TEST TOOL";

            brd.Configuration.AgencyPhone = "1234567891";






            brd.Configuration.DeliveryDetails = new hitchhiker.Contact();
            brd.Configuration.DeliveryDetails.FirstName = "Arun Balaji";
            brd.Configuration.DeliveryDetails.LastName = "Giridharan";
            brd.Configuration.DeliveryDetails.Gender = GenderEnum.Male;
            brd.Configuration.DeliveryDetails.Street = "4302 Colleg Main St";
            brd.Configuration.DeliveryDetails.City = "Bryan";
            brd.Configuration.DeliveryDetails.ZipCode = "77801";
            brd.Configuration.DeliveryDetails.Country = "us";
            brd.Configuration.DeliveryDetails.EMail = "abc@abc.com";
            brd.Configuration.DeliveryDetails.PhoneHome = "1234567891";


            brd.Configuration.ClientBusinessPhone = "1234567891";
            brd.Configuration.ClientHomePhone = "1234567891";
            brd.Configuration.LanguageCode = "en";

            brd.Configuration.WebFare = new BookRequestWebFare();
            brd.Configuration.WebFare.IsLiveBooking = false;
            brd.Configuration.WebFare.Transactioncode = "1234561";
            brd.Configuration.WebFare.DeliveryDetails = new hitchhiker.Contact();
            brd.Configuration.WebFare.DeliveryDetails.FirstName = "Arun Balaji";
            brd.Configuration.WebFare.DeliveryDetails.LastName = "Giridharan";
            brd.Configuration.WebFare.DeliveryDetails.Gender = GenderEnum.Male;
            brd.Configuration.WebFare.DeliveryDetails.Street = "4302 Colleg Main St";
            brd.Configuration.WebFare.DeliveryDetails.City = "Bryan";
            brd.Configuration.WebFare.DeliveryDetails.ZipCode = "77801";
            brd.Configuration.WebFare.DeliveryDetails.Country = "us";
            brd.Configuration.WebFare.DeliveryDetails.EMail = "abc@abc.com";
            brd.Configuration.WebFare.DeliveryDetails.PhoneHome = "1234567891";





            brd.Currency = "USD";


            BookRequestLeg leg = new BookRequestLeg();
            leg.CarrierCode = carrier;
            leg.AvailString = "Test";


            BookRequestSegment segment = new BookRequestSegment();
            segment.Departure = departure;
            segment.DepartureDateTime = departureDate;
            segment.Carrier = carrier;
            leg.Segments = new BookRequestSegment[] { segment };

            brd.Legs = new BookRequestLeg[] { leg };

            BookRequestPassenger passenger = new BookRequestPassenger();
            passenger.FirstName = "Arun Balaji";
            passenger.LastName = "Giridharan";
            passenger.PassengerType = PassengerTypeEnum.Adult;
            passenger.IDCardNumber = "12345";
            passenger.Gender = GenderEnum.Male;
            passenger.AssistanceRequired = false;
            passenger.BoardingDetails = new PassengerBoarding[] { new PassengerBoarding() };
            passenger.Title = "Mr.";
            passenger.APIS = new APISDetails();
            passenger.APIS.FirstName = "Arun Balaji";
            passenger.APIS.LastName = "Giridharan";
            passenger.APIS.DateOfBirth = new DateTime(1990, 11, 26);
            passenger.APIS.Gender = "Male";

            brd.Passengers = new BookRequestPassenger[] { passenger };

            brd.Payment = new BookRequestPayment();
            brd.Payment.PaymentType = PaymentTypeEnum.CreditCard;
            brd.Payment.BankAccountHolderFirstName = "Arun";
            brd.Payment.BankAccountHolderLastName = "Giridharan";
            brd.Payment.BankAccountNumber = "120908808";
            brd.Payment.BankName = "ABC Bank";
            brd.Payment.CountryCode = "us";
            brd.Payment.CreditCardHolderFirstName = "Arun";
            brd.Payment.CreditCardHolderLastName = "Giridharan";
            brd.Payment.CreditCardNumber = "127817928791";
            brd.Payment.CreditCardNumberIsCrypt = true;
            brd.Payment.CreditCardNumberIsCryptSpecified = true;
            brd.Payment.CreditCardExpiryDate = new DateTime(2016, 10, 10);
            brd.Payment.CreditCardExpiryDateSpecified = true;
            brd.Payment.CreditCardCVCNumber = "123";
            brd.Payment.CreditCardCVCNumberIsCrypt = true;
            brd.Payment.CreditCardCVCNumberIsCryptSpecified = true;
            brd.Payment.CreditCardType = "Visa";
            brd.Payment.CreditCardUserName = "abg";



            brd.Source = SourceModuleEnum.WebSource;
            brd.TicketingType = TicketingTypeEnum.ElectronicTicketing;
            brd.TicketingTypeSpecified = true;
            brd.IgnoreCCError = true;
            brd.IgnoreCCErrorSpecified = true;

            return fapi.BookFare(login, brd);
        }




    }
}
