﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace FlightAPIApplication
{
    public class StandardRequest
    {
        public int TravellersAdults { get; set; }

        public int TravellersKids { get; set; }

        public int TravellersSeniors { get; set; }

        public int TravellersInfants { get; set; }

        public ProductSubTypes ProductSubType { get; set; }

        public enum ProductSubTypes
        {
            OneWay,
            Return,
            MultiCity,
            OpenJaw
        }

        public DateTime DepartureDate { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public hitchhiker.ClassTypeEnum Class { get; set; }

        public DateTime ArrivalDate { get; set; }

        public List<StandardRequest> MultipleLegs { get; set; }
    }
}
